package com.runt.colegio.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.runt.colegio.entidad.Asignatura;
import com.runt.colegio.entidad.Estudiante;

@Repository
public interface EstudianteRepositorio extends JpaRepository<Estudiante, Long> {
	
	public List<Estudiante> findByAsignaturas(Asignatura asignaturas);

}
