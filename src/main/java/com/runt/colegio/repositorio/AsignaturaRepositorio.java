package com.runt.colegio.repositorio;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.runt.colegio.entidad.Asignatura;
import com.runt.colegio.entidad.Profesor;

@Repository
public interface AsignaturaRepositorio extends JpaRepository<Asignatura, Long> {

	public List<Asignatura> findByProfesor(Profesor profesor);
}
