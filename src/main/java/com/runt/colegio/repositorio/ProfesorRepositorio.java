package com.runt.colegio.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.runt.colegio.entidad.Profesor;

@Repository
public interface ProfesorRepositorio extends JpaRepository<Profesor, Long> {



}
