package com.runt.colegio.servicio;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runt.colegio.entidad.Asignatura;
import com.runt.colegio.entidad.Estudiante;
import com.runt.colegio.entidad.Profesor;
import com.runt.colegio.repositorio.AsignaturaRepositorio;
import com.runt.colegio.repositorio.ProfesorRepositorio;

@Service
public class AsignaturaServicio {

	@Autowired
	private AsignaturaRepositorio asignaturaRepositorio;

	@Autowired
	private ProfesorRepositorio profesorRepositorio;

	public List<Asignatura> obtenerAsignaturaPorProfesor(Long profesorId) {
		Optional<Profesor> profesor = profesorRepositorio.findById(profesorId);
		List<Asignatura> asignaturas;

		if (profesor.isPresent()) {
			asignaturas = asignaturaRepositorio.findByProfesor(profesor.get());
		} else {
			asignaturas = null;
		}

		return asignaturas;
	}
	
	public Optional<Asignatura> obtenerAsignaturaPorId(Long id) {
		Optional<Asignatura> asignatura = null;
		if(id != null) {
			asignatura = asignaturaRepositorio.findById(id);
		}
		return asignatura;
	}
}
