package com.runt.colegio.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.runt.colegio.entidad.Profesor;
import com.runt.colegio.repositorio.ProfesorRepositorio;

@Service
public class ProfesorServicio {

	@Autowired
	private ProfesorRepositorio profesorRepositorio;

	public List<Profesor> obtenerListaProfesores() {
		List<Profesor> profesor = profesorRepositorio.findAll();
		return profesor;

	}

}
