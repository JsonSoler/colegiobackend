package com.runt.colegio.controlador;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.runt.colegio.entidad.Asignatura;
import com.runt.colegio.entidad.Estudiante;
import com.runt.colegio.servicio.AsignaturaServicio;

@RestController
@RequestMapping("asignatura")
public class AsignaturaControlador {

	@Autowired
	private AsignaturaServicio asignaturaServicio;

	@GetMapping(value = "profesor/{profesor_id}")
	public ResponseEntity<List<Asignatura>> obtenerAsignaturaPorProfesor(@PathVariable("profesor_id") Long profesorId) {

		List<Asignatura> asignaturas = asignaturaServicio.obtenerAsignaturaPorProfesor(profesorId);
		if (asignaturas != null) {
			return ResponseEntity.ok(asignaturas);
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	@GetMapping(value = "{idAsignatura}/estudiantes")
	public ResponseEntity<List<Estudiante>> obtenerEstudiantesPorAsignatura(@PathVariable("idAsignatura") Long asignaturaId) {

		Optional<Asignatura> asignatura = asignaturaServicio.obtenerAsignaturaPorId(asignaturaId);
		if (asignatura.isPresent()) {
			List<Estudiante> estudiantes = asignatura.get().getEstudiantes();
			return ResponseEntity.ok(estudiantes);
		} else {
			return ResponseEntity.noContent().build();
		}
	}
}
