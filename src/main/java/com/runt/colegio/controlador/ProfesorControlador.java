package com.runt.colegio.controlador;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.runt.colegio.entidad.Profesor;
import com.runt.colegio.servicio.ProfesorServicio;

@RestController
@RequestMapping("profesores")
public class ProfesorControlador {

	@Autowired
	private ProfesorServicio profesorServicio;

	@GetMapping
	public ResponseEntity<List<Profesor>> obtenerListaProfesores() {
		List<Profesor> profesor = profesorServicio.obtenerListaProfesores();
		return ResponseEntity.ok(profesor);
	}

}
