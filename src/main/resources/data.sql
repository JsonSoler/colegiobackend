INSERT INTO Colegio (id,nombre) VALUES (1,'El colegio del Olimpo');

INSERT INTO Curso (id,grado,salon,colegio_id) VALUES (1,10,'A',1),(2,10,'B',1),(3,11,'A',1),(4,11,'B',1);

INSERT INTO Profesor (id,nombre) 
VALUES (1,'Nemesis'),(2,'Priapo'),(3,'Iris');

INSERT INTO  Asignatura (id,nombre,curso_id,profesor_id)
VALUES (1,'Matematicas',1,1),
(2,'Español',1,2),
(3,'Ingles basico',1,3),

(4,'Matematicas',2,1),
(5,'Español',2,2),
(6,'Ingles Avanzado',1,3),

(7,'Matematicas',3,1),
(8,'Pre Icfes',3,1),

(9,'Matematicas',4,1),
(10,'Pre Icfes',4,1);

INSERT INTO Estudiante(id,nombre) 
VALUES (1,'Afrodita'),
(2,'Apolo'),
(3,'Ares'),
(4,'Artemisa'),
(5,'Atenea'),
(6,'Dionisio'),
(7,'Hefesto'),
(8,'Hera'),
(9,'Hermes'),
(10,'Hades'),
(11,'Poseidon'),
(12,'Zeus');


INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (1,1);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (1,2);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (1,3);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (2,1);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (2,2);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (2,3);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (3,1);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (3,2);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (3,3);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (4,4);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (4,5);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (4,6);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (5,4);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (5,5);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (5,6);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (6,4);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (6,5);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (6,6);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (7,7);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (7,8);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (8,7);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (8,8);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (9,9);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (9,10);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (10,9);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (10,10);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (11,9);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (11,10);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (12,9);
INSERT INTO Estudiantes_Asignaturas (estudiante_id,asignatura_id) VALUES (12,10);
